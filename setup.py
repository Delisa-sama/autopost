# coding: utf-8
from __future__ import print_function, unicode_literals

import codecs
import sys

from setuptools import setup, find_packages

from src import __version__, __author__, __email__

with open('requirements.txt') as f:
    requirements = [l for l in f.read().splitlines() if l]


def long_description():
    with codecs.open('README.md', 'rb') as f:
        if sys.version_info >= (3, 0, 0):
            return str(f.read())


setup(
    name='autopost',
    version=__version__,
    packages=find_packages(),
    author=__author__,
    author_email=__email__,
    long_description=long_description(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requirements,
    license='ZLOdeuka-',
)
