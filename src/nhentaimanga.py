import sys

from src import constant
from src.manga import Manga
from src.utils import request, join


class NhentaiManga(Manga):
    def __init__(self, nhentai_id):
        Manga.__init__(self)
        self.id = int(nhentai_id)
        self.nhentai_url = '{0}/{1}'.format(constant.DETAIL_URL, self.id)
        try:
            response = request('get', self.nhentai_url, constant.PROXY).json()
        except Exception as e:
            self.logger.error(str(e))
            sys.exit(1)

        self.title = response['title']['english']
        self.pages = len(response['images']['pages'])
        self.tags = []
        self.artist = []
        self.language = []

        for tag in response['tags']:
            tag_name = '#' + join(tag['name'].split(' '), '_').replace('-', '_')
            if tag['type'] == 'tag':
                self.tags.append(tag_name)
            elif tag['type'] == 'artist':
                self.artist.append(tag_name)
            elif tag['type'] == 'language':
                self.language.append(tag_name)

        self.ext = ''.join(map(lambda s: s['t'], response['images']['pages']))

        self.img_id = response['media_id']

    def __str__(self):
        return "[- {title} -]({link})\nTags: {tags}\nArtist: {artist}\nLanguage: {language}\nPages: {pages}".format(
            title=self.title.replace('[', '').replace(']', ''),
            link=self.telegraph_url,
            tags=self.get_tags(),
            artist=self.get_artist(),
            language=self.get_language(),
            pages=self.pages
        )

    def get_tags(self):
        return join(self.tags, ' ')

    def get_artist(self):
        return join(self.artist, ' ')

    def get_language(self):
        return join(self.language, ' ')
