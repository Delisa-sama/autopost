import os
from time import gmtime, strftime

import colorama

from src import constant
from src.utils import Singleton


class Logger(Singleton):
    def __init__(self, path=constant.LOG_PATH):
        colorama.init()
        if not os.path.exists(path):
            os.mkdir(path)
        self.log_file = open('{}/{}'.format(path, "log.txt"), mode="a+", encoding="utf-8")

    def _print(self, type, msg, bg_color=constant.NORMAL_BG_COLOR, fg_color=constant.NORMAL_FG_COLOR):
        date = strftime("%Y-%m-%d", gmtime())
        print(constant.MSG_TYPE_BG_COLOR + constant.MSG_TYPE_FG_COLOR
              + '[' + type + '] ' + date + ' ' + bg_color + fg_color + msg
              )
        print(type + ' ' + date + ' ' + msg, file=self.log_file)

    def info(self, msg):
        self._print("INFO", msg, bg_color=constant.INFO_BG_COLOR, fg_color=constant.INFO_FG_COLOR)

    def error(self, msg):
        self._print("ERROR", msg, bg_color=constant.ERROR_BG_COLOR, fg_color=constant.ERROR_FG_COLOR)

    def complete(self, msg):
        self._print("COMPLETE", msg, bg_color=constant.COMPLETE_BG_COLOR, fg_color=constant.COMPLETE_FG_COLOR)

    @staticmethod
    def send_nudes():
        print(constant.NUDES_BG_COLOR + constant.NUDES_FG_COLOR + """
    
    
                                         ii. :::
                                    ri,:iiiii;ir::
                                    ,ri:::::::::i;;ri.
                                   ivi::::::::::::::i:.
                                7uu7i:iir:::::::::::::,
                                 iL7ir77;7L7ri;r:,:ir7,
                                  vrr7r1v.JYYYvvvi7v7vi
                                 rjvu:iU5uPvvrY.YkY7,77
                                 :7YNS  ..    . .0Ji.7r
                                   jki     .      .1rvi
                                   :U:,.    ...   7UUY:
                                     i.,,:   .   rL vU
                                      ..iBP     1k, i;
                                     .  :L.   U@5:.. .
                                    .. .r  iMB@r,i. . .
                                    .  : . 7Zi..:7. .. ,:i
                                    :::     ;.   i. ..  i.
                                 ,.,:v    rXLi   :   .,:
                                 ii::,    :;.:k .. ..,. .,  .
                                ::.7i:     .: i:i.ir.. ,r  .
                               ,5,ir,.    .:.i:ir::,.  :: .
                              ..v:vi..   . :, . :,:.   7. ..
                              ,  ir.. ,  .,   .,::     7. .i          .
                              :  :r,  7   i.  .r.     .7,.ii          :
                             .. .Fu   ir   v. ,Y      ::r:i.       .. :
                             i,:vP     Li  :7  L     .,i5i:.      .:  .
                            i,..Y.  ,: .ji  ;i .     70Zki:       r   :
                           .:  :r     ..,2: .,    iq7@qE7:        r  ;,
                          .:  .7:      :::@S1;LkO@@BZSO,S        .i  r
                         .:.  :7      .vi .@@@B@B@@OM1F27 .:::::,;: ii
                        .:. ..rr     :7:   :@B@BO51uGPrvZvrii::ir;v 7i
                       .,.  .:7Li. ,77      i81PPvi::..7r:.       B@:
                      ,,... .:viirOL:        :;:i   :,rvY:  . .  ,U:
                     :.. .  :;7riiX,          rr:: .::7Lj, . ..  u,
                    ,:...  .r7v:               vi:::::vYY,  ... ri
                    ., ....rr:                 r7::::77Jj: ... .L .
                    ,.. :ii.                   7F::,ivi2; ...  Y:.,
                    .i,:.                      :vv,:7v:.   .  jYiv.
                       .                       Yir:iL.  .    iS::
                                             . N:::v.  .   ::Li.
                       .                     r r::r:  .   ,u7,i:
                                             i,::ir  .   :L7,ri
                                           .:.r:ii: .   :L7i7i      .
                                   .   ..: i,:iii:    .r5u7r,       ,.
                                 :uL7.  :vii.i:;,   .:LSi..          r:
                             .:,,::irF   vr:,r7.   .r7i        .     ..
                           .,.. .  .iiJ  :i.i1i   :LLi:...        ..
                         ....... .  ,i7: i,iBZFE7ir;::.,.,.,,,.    .,.
                      ... ...,,...  .ii..iMOPMOB@Jir777ii::..  .,     .
                     . ..... ..,   .:::::OB@Yi@Bkri:.....,,:ir:
                  ... .......   .,iii,7O@B@7U@@7             ,7ri     .
                 ... ..,..    .:i;:::vB5..:PMM.    . .....   :ii;7.  .
              ......... .   ,:ii::i7Li     rJ   . . .....   :ii:i:v: ,
             ,.........  .::ii:ir7r:     iv,   .........  .iii:i:::7:
           .,.... .   ,:;ri:::7ri, .:. :L;.   ... ... .  :;ii:::::iY7
          ........ .,i::.  ,::vri,:: ,LY,  . . . ...   ,i;:i:i::r7v
    . .. :, ..... ..,   .,:i:,::rLr rui   . .......   ir:::::,r17::.
    .   ,: . ...     .,iii:::rUr:7J2r,   . ..... ....:,.,::::vL:::,i
       .: ...    .,,i:i:::rvjv:,UB2,  . ..... . ...   .:i::rjvirrvS
       :.     .,:iiriiivL2ujvLk@BL   ... . .   ::.  ,ii::rYji:         .
    . ..:  .,iJvv7YLJJU7i...;J@B.   . ...   ,:r:. .:i::7YL:
    .   :Gr77r7L07.  .i.           ...   .:ii:   .:iiuFJui
    ..::::.     .::.,...,,,:7.    ... .,::,     :i7JUi    :vLi::,.........,.,..
                   ....,,. v.  . . .....     .:rvu7,        .:::......
                . .   .   ;,  ... ..      ,:rr7J7.
         . . . . ...,.    L. ... .:. .,:ivvLv1k:  ..
                          2,    ,;ii7rvUSUvi7i      .
                   . ... .rri.  :ii:    ..                  ..., .......,, ..
    : . . . . .   ...     .:;v5MN:          .:iii::.. . .   ...............,,.
        """)
