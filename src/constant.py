# coding: utf-8
from __future__ import unicode_literals, print_function

import os

from colorama import Fore, Back

from src.utils import urlparse

dir_path = os.getcwd()

image_ext = [".png", ".jpg", ".jpeg"]

UPLOAD_URL = 'https://telegra.ph/upload'

TELEGRAPH_URL = 'http://telegra.ph/'

FIRST_UNIMPORTANT_PAGES = 3
LAST_UNIMPORTANT_PAGES = 3

LOG_PATH = dir_path + os.path.sep + "logs"
DOWNLOAD_PATH = dir_path + os.path.sep + "downloads"

BASE_URL = 'https://nhentai.net'

DETAIL_URL = '%s/api/gallery' % BASE_URL
SEARCH_URL = '%s/api/galleries/search' % BASE_URL
LOGIN_URL = '%s/login/' % BASE_URL
FAV_URL = '%s/favorites/' % BASE_URL

u = urlparse(BASE_URL)
IMAGE_URL = '%s://i.%s/galleries' % (u.scheme, u.hostname)

CONFIG_PATH = dir_path + os.path.sep + 'config.ini'

PROXY = {}

DOWNLOAD_THREADS = 5
UPLOAD_THREADS = 5

# Просто сообщения
NORMAL_BG_COLOR = Back.BLACK
NORMAL_FG_COLOR = Fore.LIGHTYELLOW_EX
# Тип сообщения
MSG_TYPE_BG_COLOR = Back.BLACK
MSG_TYPE_FG_COLOR = Fore.LIGHTCYAN_EX
# Ошибки
ERROR_BG_COLOR = Back.BLACK
ERROR_FG_COLOR = Fore.RED
# Сообщение о завершении
COMPLETE_BG_COLOR = Back.BLACK
COMPLETE_FG_COLOR = Fore.GREEN
# ИНФО
INFO_BG_COLOR = Back.BLACK
INFO_FG_COLOR = Fore.WHITE

NUDES_BG_COLOR = Back.BLACK
NUDES_FG_COLOR = Fore.LIGHTCYAN_EX
