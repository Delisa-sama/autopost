import sys

from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import *


class Task(QWidget):
    def __init__(self, manga, parent=None, w=400, h=300):
        super(Task, self).__init__(parent)
        self.setFixedSize(w, h)
        self.progress = QProgressBar()

        self.manga = manga

        self.preview = QLabel()
        self.preview.setFixedSize((w / 2).__int__(), h)

        self.result = QTextEdit()
        self.result.setPlaceholderText("Result")
        self.result.setMinimumWidth(75)

        self.title = QLabel("Manga Title")

        self.copy_btn = QPushButton("COPY")
        self.copy_btn.clicked.connect(self._copy_result)

        hbox = QHBoxLayout()
        hbox.addWidget(self.preview)

        vbox = QVBoxLayout()

        vbox.addWidget(self.title)
        vbox.addWidget(self.progress)
        vbox.addWidget(self.result)
        vbox.addWidget(self.copy_btn)

        hbox.addLayout(vbox)

        self.clipboard = QApplication.clipboard()

        self.setLayout(hbox)

    def _copy_result(self):
        self.clipboard.clear()
        self.clipboard.setText(self.result.toPlainText())

    def set_preview(self, pixmap: QPixmap):
        self.preview.setPixmap(pixmap.scaled(self.preview.size(), QtCore.Qt.KeepAspectRatio))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = Task()
    win.show()
    sys.exit(app.exec_())
