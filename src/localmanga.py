import os

from src.manga import Manga


class LocalManga(Manga):
    def __init__(self, path, title=None):
        Manga.__init__(self)
        if title is None:
            title = os.path.dirname(path).split(os.path.sep)[-1]

        self.local_path = path
        self.title = title
        self.pages = len(os.listdir(self.local_path))

    def __str__(self):
        return "[- {title} -]({link})\nPages: {pages}".format(
            title=self.title,
            link=self.telegraph_url,
            pages=self.pages
        )
