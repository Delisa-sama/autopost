import configparser
import os
import subprocess
import sys
from urllib import parse

from PyQt5.QtWidgets import *
from telegraph import Telegraph, exceptions

from src import constant
from src import logger
from task import Task
from src.localmanga import LocalManga
from src.nhentaimanga import NhentaiManga
from src.utils import urlvalidator


class Window(QWidget):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        self.link = None
        self.result = None
        self.post_btn = None
        self.paste_btn = None
        self.manga = None

        self.copy_btn = None
        try:
            self.logger = logger.Logger()
        except:
            print("LOGGER creation failed")
            exit(1)

        self.config = configparser.ConfigParser()
        with open(constant.CONFIG_PATH, mode="r") as f:
            self.config.read_file(f)

        if not self.config.sections():
            self.logger.error("Config parse error: Sections not found")
            exit(1)

        try:
            self.channel = self.config.get("TELEGRAM", "channel")
        except configparser.NoSectionError:
            self.logger.error("Section TELEGRAM not found in config file")
            exit(1)

        grid = QGridLayout()

        self._create_task_container()

        grid.addWidget(self._create_control_group(), 0, 1)
        grid.addWidget(self.scroll_area, 0, 0)
        # grid.addWidget(self._create_task_group(), 1, 1)

        self.setLayout(grid)

        self.clipboard = QApplication.clipboard()
        self.setWindowTitle("Autoposter")

        try:
            self.resize(self.config.getint("WINDOW", "width"), self.config.getint("WINDOW", "height"))
        except configparser.NoSectionError:
            self.logger.error("Section WINDOW not found in config file")
            exit(1)
        except configparser.NoOptionError:
            self.logger.error("Option width or height not found in config file")
            exit(1)

    def _paste_link(self):
        self.link.setText(self.clipboard.text())

    def _cpy_msg(self):
        self.clipboard.clear()
        self.clipboard.setText(self.result.toPlainText())

    def _manga_post(self):
        if self.nhentai_btn.isChecked():
            if urlvalidator(self.link.text()):
                self.url = self.link.text()
            else:
                self.logger.error("URL is not valid")
                return

            id = str(parse.urlsplit(self.url).path).replace('/', '').replace('g', '')

            if not id.isdigit():
                self.logger.error("id must be digit")
                return

            self.manga = NhentaiManga(id)

            os.makedirs(constant.DOWNLOAD_PATH, exist_ok=True)
            command = ['nhentai', '--id', str(self.manga.id), '--output',
                       constant.DOWNLOAD_PATH + os.path.sep + str(self.manga.id),
                       '--nohtml']

            process = subprocess.Popen(command)
            process.wait()
            process.kill()

            download_path = constant.DOWNLOAD_PATH + os.path.sep + str(self.manga.id)
            manga_folder = os.listdir(download_path)[0]
            self.manga.local_path = download_path + os.path.sep + manga_folder

        elif self.local_btn.isChecked():
            if self.link.text() is '':
                self.logger.error("Path must be not empty")
                return

            path = self.link.text() + os.path.sep
            if not os.path.exists(path):
                self.logger.error("Folder is not exists")
                return

            self.manga = LocalManga(path=path, title=None if self.manga_title.text() == '' else self.manga_title.text())

        self.manga.set_logger(self.logger)

        self._post()

    def _post(self):
        if not self.manga:
            self.logger.error("Manga must be not None")
            return

        try:
            self.telegraph = Telegraph()
            self.telegraph.create_account(short_name=self.config.get("TELEGRAM", "telegraph_account"))
        except exceptions.TelegraphException as e:
            self.logger.error("Telegraph error, check your connection to telegra.ph " + str(e))

        self.manga.upload()
        self.manga.generate_html()
        self.manga.post(self.telegraph, self.channel)
        self.result.setText(str(self.manga))
        self.logger.complete(str(self.manga))

        # try:
        #     tg = Telegram(self.config)
        #     tg.post(self.manga.__str__(), "15:00")
        # except errors.TimeoutError:
        #     self.logger.error("Telegram timeout")
        #     return
        # except errors as e:
        #     self.logger.error(str(e))
        #     return

    def _clear(self):
        self.manga_title.clear()
        self.link.clear()
        self.result.clear()

    def _create_task_container(self):
        self.scroll_area = QScrollArea()
        self.scroll_area.setFixedWidth(450)
        self.scroll_area.setWidgetResizable(True)

        widget = QWidget()
        self.scroll_area.setWidget(widget)
        self.layout_SArea = QVBoxLayout(widget)

        self.tasks = []

        self._add_task(Task())
        self._add_task(Task())
        self._add_task(Task())

    def _add_task(self, task):
        self.layout_SArea.addWidget(task)
        self.tasks.append(task)

    def _create_control_group(self):
        group_box = QGroupBox("Input")

        self.local_btn = QRadioButton("Local")
        self.nhentai_btn = QRadioButton("nHentai")
        self.type = QHBoxLayout()
        self.nhentai_btn.setChecked(True)

        self.type.addWidget(self.nhentai_btn)
        self.type.addWidget(self.local_btn)

        self.link = QLineEdit()
        self.link.setPlaceholderText("Link")
        self.manga_title = QLineEdit()
        self.manga_title.setPlaceholderText("Title")

        self.post_btn = QPushButton("POST")
        self.post_btn.clicked.connect(self._manga_post)

        self.paste_btn = QPushButton("PASTE")
        self.paste_btn.clicked.connect(self._paste_link)

        self.clear_btn = QPushButton("CLEAR")
        self.clear_btn.clicked.connect(self._clear)

        vbox = QVBoxLayout()
        vbox.addWidget(self.manga_title)
        vbox.addWidget(self.link)
        vbox.addLayout(self.type)

        hbox = QHBoxLayout()
        hbox.addWidget(self.paste_btn)
        hbox.addWidget(self.post_btn)
        hbox.addWidget(self.clear_btn)

        vbox.addLayout(hbox)

        group_box.setLayout(vbox)

        return group_box

    def _create_task_group(self):
        group_box = QGroupBox("Output")

        self.result = QTextEdit()
        self.result.setPlaceholderText("Result message")
        self.copy_btn = QPushButton("Copy")
        self.copy_btn.clicked.connect(self._cpy_msg)

        vbox = QVBoxLayout()

        vbox.addWidget(self.result)
        vbox.addWidget(self.copy_btn)

        group_box.setLayout(vbox)

        return group_box


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = Window()
    win.show()
    sys.exit(app.exec_())
