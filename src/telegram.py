from telethon import TelegramClient, errors


class Telegram:
    def __init__(self, config):
        self.tg = TelegramClient(
            session='some_session',
            api_hash=config.get("TELEGRAM", "api_hash"),
            api_id=config.get("TELEGRAM", "api_id")
        )

        self.tg.start()
        self.chat_id = config.get("TELEGRAM", "bot_name")
        self.offset = 1  # 1 hour

    def post(self, content, time):
        with self.tg.conversation(self.chat_id) as conv:
            conv.send_message("/start")
            res = conv.get_response()
            res.click(text=res.reply_markup.rows[0].buttons[0].text)  # create post

            res = self.tg.get_messages(conv.chat_id, 1)[0]
            res.click(text=res.reply_markup.rows[0].buttons[0].text)  # select Lolimanga

            res = conv.get_response()
            conv.send_message(content)  # send msg

            # res = self.tg.get_messages(conv.chat_id, 1)[0]
            # print("content sended")
            # conv.send_message("Далее")  # next
            # print("Далее")
            #
            # res = conv.get_response()
            # res = self.tg.get_messages(conv.chat_id, 1)[0]
            # res.click(text="Отложить")
            # print("Отложить")
            #
            # res = conv.get_response()
            # res = self.tg.get_messages(conv.chat_id, 1)[0]
            # print(res)
            # conv.send_message(time)
            # print(time)
            #
            # res = self.tg.get_messages(conv.chat_id, 1)[0]
            # conv.send_message("Далее")
            # print("Далее")
            #
            # res = conv.get_response()
