import json
import os
from multiprocessing.dummy import Pool as ThreadPool

import requests
from telegraph import TelegraphException

from src import constant


class Manga:
    def __init__(self):
        self.local_path = None
        self.title = None
        self.pages = 0
        self.images_urls = []
        self.images_paths = []
        self.telegraph_url = ''
        self.html_content = ''
        self.logger = None

    def set_logger(self, log):
        self.logger = log

    def __str__(self):
        pass

    def upload(self):
        def _upload(file):
            res = None
            url = None

            while not res:
                try:
                    res = requests.post(constant.UPLOAD_URL, timeout=None,
                                        files={'file': ('file', open(file, 'rb'), 'image/jpeg')}).json()
                except requests.exceptions.Timeout:
                    self.logger.error("Timeout")
                except requests.exceptions.TooManyRedirects:
                    self.logger.error("Too Many Redirects")
                except requests.exceptions.RequestException as e:
                    self.logger.error("Some fatal error" + str(e))

            try:
                url = res[0]['src']
            except KeyError:
                self.logger.error(
                    "Failed to upload file {file}: {reason}".format(file=os.path.basename(file), reason=res["error"]))
                return file, None

            self.logger.complete("Uploaded file: " + os.path.basename(file) + "; JSON: " + json.dumps(res))
            return file, res[0]['src']

        # END _upload

        images_paths = [
            self.local_path + os.path.sep + f
            for f in os.listdir(self.local_path)
            if f.endswith(tuple(constant.image_ext)) and f.split('.')[0].isdigit()
        ]
        self.logger.info("Uploading manga to telegra.ph")
        pool = ThreadPool(constant.UPLOAD_THREADS)
        results = pool.map(_upload, images_paths)
        pool.close()
        pool.join()

        def _sort_filename_int(tup):
            return int(os.path.basename(tup[0]).split('.')[0])

        results = [s for s in results if s[1] is not None]
        results.sort(key=_sort_filename_int)

        if self.pages != len(results):
            self.logger.error("Some pages were not loaded")
            self.pages = len(results)

        for result in results:
            self.images_urls.append(result[1])

    def generate_html(self):
        for i in range(0, len(self.images_urls)):
            try:
                self.html_content += "<img src='{}'/>".format(self.images_urls[i])
            except TypeError as e:
                self.logger.error(str(e))
                if (i in range(0, constant.FIRST_UNIMPORTANT_PAGES)
                        or i in range(len(self.images_urls) - constant.LAST_UNIMPORTANT_PAGES,
                                      len(self.images_urls))):
                    continue

    def post(self, telegraph, channel):
        self.logger.info('Posting to telegra.ph')
        try:
            response = None
            while not response:
                response = telegraph.create_page(self.title, html_content=self.html_content, author_name=channel)
        except TelegraphException:
            self.logger.error('Telegraph error')
            return None
        self.telegraph_url = constant.TELEGRAPH_URL + response['path']
